package dev.phillip;

public class HelloLambda {

    public String handleRequest() {
        System.out.println("Hello Lambda");
        return "Hello AWS Lambda";
    }

}
